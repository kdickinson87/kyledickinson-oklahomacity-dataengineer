USE [FracFocusStaging]
GO

/****** Object:  View [dbo].[vw_OCCWellAttributes]    Script Date: 9/14/2019 8:07:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vw_OCCWellAttributes]
AS
     SELECT GETDATE() AS DataDate,
		    REPLACE(REPLACE([API_Number], '"', ''), '''', '') AS [APINumber], 
            MAX(LTRIM(RTRIM(REPLACE(REPLACE([Well_Name], '"', ''), '''', '')))) AS WellName, 
            MAX(LTRIM(RTRIM(REPLACE(REPLACE([Well_Number], '"', ''), '''', '')))) AS WellNumber, 
            MAX(REPLACE([Well_Status], '"', '')) AS WellStatus, 
            MAX([Spud]) AS Spud, 
            max([First_Prod]) AS FirstProd
     FROM [FracFocusStaging].[dbo].[W27base]
	 GROUP BY API_Number;
GO


