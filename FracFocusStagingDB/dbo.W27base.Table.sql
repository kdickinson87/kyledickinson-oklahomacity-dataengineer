USE [FracFocusStaging]
GO
/****** Object:  Table [dbo].[W27base]    Script Date: 9/13/2019 10:19:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[W27base](
	[ImportGUID] [uniqueidentifier] NULL,
	[API_Number] [varchar](500) NULL,
	[Well_Name] [varchar](max) NULL,
	[Well_Number] [varchar](500) NULL,
	[Well_Status] [varchar](500) NULL,
	[Spud] [date] NULL,
	[First_Prod] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
