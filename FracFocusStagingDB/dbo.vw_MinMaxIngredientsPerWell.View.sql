USE [FracFocusStaging]
GO
/****** Object:  View [dbo].[vw_MinMaxIngredientsPerWell]    Script Date: 9/13/2019 10:19:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_MinMaxIngredientsPerWell]
AS
     SELECT DISTINCT 
            DataDate, 
            MinMax.APINumber, 
            MinPercentage, 
            MinName.IngredientName AS MinIngredient, 
            MaxPercentage, 
            MaxName.IngredientName AS MaxIngredient
     FROM
     (
         SELECT GETDATE() AS DataDate, 
                ru.APINumber, 
                MIN(ri.PercentHFJob) AS MinPercentage, 
                MAX(ri.PercentHFJob) AS MaxPercentage
         FROM FracFocusRegistry.dbo.RegistryUpload ru
              JOIN FracFocusRegistry.[dbo].[RegistryUploadIngredients] ri ON ru.pkey = ri.pKeyDisclosure
              LEFT JOIN FracFocusRegistry.dbo.RegistryUploadPurpose rp ON ri.pKeyPurpose = rp.pKey
         WHERE ri.PercentHFJob > 0
         GROUP BY ru.APINumber
     ) MinMax
     INNER JOIN
     (
         SELECT ru.APINumber, 
                ri.IngredientName, 
                ri.PercentHFJob
         FROM FracFocusRegistry.dbo.RegistryUpload ru
              JOIN FracFocusRegistry.[dbo].[RegistryUploadIngredients] ri ON ru.pkey = ri.pKeyDisclosure
              LEFT JOIN FracFocusRegistry.dbo.RegistryUploadPurpose rp ON ri.pKeyPurpose = rp.pKey
     ) MinName ON MinName.APINumber = MinMax.APINumber
                  AND MinName.PercentHFJob = MinMax.MinPercentage
     INNER JOIN
     (
         SELECT ru.APINumber, 
                ri.IngredientName, 
                ri.PercentHFJob
         FROM FracFocusRegistry.dbo.RegistryUpload ru
              JOIN FracFocusRegistry.[dbo].[RegistryUploadIngredients] ri ON ru.pkey = ri.pKeyDisclosure
              LEFT JOIN FracFocusRegistry.dbo.RegistryUploadPurpose rp ON ri.pKeyPurpose = rp.pKey
     ) MaxName ON MaxName.APINumber = MinMax.APINumber
                  AND MaxName.PercentHFJob = MinMax.MaxPercentage;
GO
