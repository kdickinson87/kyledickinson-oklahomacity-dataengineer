USE [FracFocusStaging]
GO
/****** Object:  Table [dbo].[RegistryUploadPurpose]    Script Date: 9/13/2019 10:19:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistryUploadPurpose](
	[pKey] [uniqueidentifier] NOT NULL,
	[pKeyRegistryUpload] [uniqueidentifier] NOT NULL,
	[TradeName] [varchar](250) NULL,
	[Supplier] [varchar](150) NULL,
	[Purpose] [varchar](250) NOT NULL,
	[SystemApproach] [bit] NULL,
	[IsWater] [bit] NULL,
	[PercentHFJob] [float] NULL,
	[IngredientMSDS] [bit] NULL,
	[ImportGUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_RegistryUploadPurpose] PRIMARY KEY NONCLUSTERED 
(
	[pKey] ASC,
	[ImportGUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_ImportGUID]    Script Date: 9/13/2019 10:19:33 PM ******/
CREATE NONCLUSTERED INDEX [IX_ImportGUID] ON [dbo].[RegistryUploadPurpose]
(
	[ImportGUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
