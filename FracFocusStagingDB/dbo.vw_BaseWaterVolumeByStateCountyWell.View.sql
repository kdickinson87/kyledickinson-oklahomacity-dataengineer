USE [FracFocusStaging]
GO
/****** Object:  View [dbo].[vw_BaseWaterVolumeByStateCountyWell]    Script Date: 9/13/2019 10:19:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_BaseWaterVolumeByStateCountyWell]
AS
     SELECT GETDATE() AS DataDate, 
            SCR.State, 
            SCR.County, 
            APINumber, 
            SUM(ISNULL(TotalBaseWaterVolume, 0)) AS TotalBaseWaterVolume
     FROM RegistryUpload RU
          INNER JOIN StateCountyRef SCR ON SCR.State_API_Code = RU.StateNumber
                                           AND SCR.County_FIPS_Code = RU.CountyNumber
     GROUP BY SCR.State, 
              SCR.County, 
              APINumber;
GO
