USE [FracFocusStaging]
GO
/****** Object:  View [dbo].[vw_JobCompletionsBySupplierWellMonth]    Script Date: 9/13/2019 10:19:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_JobCompletionsBySupplierWellMonth]
AS
     SELECT GETDATE() AS DataDate,
            CASE
                WHEN LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(rp.Supplier, CHAR(10), CHAR(32)), CHAR(13), CHAR(32)), CHAR(160), CHAR(32)), CHAR(9), CHAR(32)))) = 'Operator'
                THEN ru.OperatorName
                ELSE rp.Supplier
            END AS Supplier, 
            ru.APINumber, 
            DATEADD(mm, DATEDIFF(mm, 0, ru.JobStartDate), 0) AS JobStartMonth, 
            COUNT(DISTINCT ru.pKey) JobCount
     FROM RegistryUpload ru
          INNER JOIN RegistryUploadPurpose rp ON rp.pKeyRegistryUpload = ru.pKey
     WHERE rp.Supplier IS NOT NULL
           AND rp.Supplier NOT IN('', 'Listed Above')
     GROUP BY ru.APINumber,
              CASE
                  WHEN LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(rp.Supplier, CHAR(10), CHAR(32)), CHAR(13), CHAR(32)), CHAR(160), CHAR(32)), CHAR(9), CHAR(32)))) = 'Operator'
                  THEN ru.OperatorName
                  ELSE rp.Supplier
              END, 
              DATEADD(mm, DATEDIFF(mm, 0, ru.JobStartDate), 0); 
GO
