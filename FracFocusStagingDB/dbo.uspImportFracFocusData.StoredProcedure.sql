USE [FracFocusStaging]
GO
/****** Object:  StoredProcedure [dbo].[uspImportFracFocusData]    Script Date: 9/13/2019 10:19:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspImportFracFocusData]
AS
BEGIN

	DECLARE @ImportGUID UNIQUEIDENTIFIER = NEWID()
	
	BEGIN TRAN
	BEGIN TRY
	
		INSERT INTO RegistryUpload
		SELECT [pKey]
			 , [JobStartDate]
			 , [JobEndDate]
			 , [APINumber]
			 , [StateNumber]
			 , [CountyNumber]
			 , [OperatorName]
			 , [WellName]
			 , [Latitude]
			 , [Longitude]
			 , [Projection]
			 , [TVD]
			 , [TotalBaseWaterVolume]
			 , [TotalBaseNonWaterVolume]
			 , [StateName]
			 , [CountyName]
			 , [FFVersion]
			 , [FederalWell]
			 , [IndianWell]
			 , [Source]
			 , [DTMOD]
			 , @ImportGUID
		FROM FracFocusRegistry.dbo.RegistryUpload
	
		INSERT INTO RegistryUploadPurpose
		SELECT [pKey]
			 , [pKeyRegistryUpload]
			 , [TradeName]
			 , [Supplier]
			 , [Purpose]
			 , [SystemApproach]
			 , [IsWater]
			 , [PercentHFJob]
			 , [IngredientMSDS]
			 , @ImportGUID
		FROM FracFocusRegistry.dbo.RegistryUploadPurpose
	
		INSERT INTO RegistryUploadIngredients
		SELECT [pKey]
			 , [pKeyPurpose]
			 , [IngredientName]
			 , [CASNumber]
			 , [PercentHighAdditive]
			 , [PercentHFJob]
			 , [IngredientComment]
			 , [IngredientMSDS]
			 , [MassIngredient]
			 , [ClaimantCompany]
			 , [pKeyDisclosure]
			 , @ImportGUID
		FROM FracFocusRegistry.dbo.RegistryUploadIngredients
	
		DELETE FROM RegistryUpload WHERE ImportGUID <> @ImportGUID
		DELETE FROM RegistryUpload WHERE ImportGUID <> @ImportGUID
		DELETE FROM RegistryUpload WHERE ImportGUID <> @ImportGUID
	
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		THROW
	END CATCH
END
GO
