USE [FracFocusStaging]
GO
/****** Object:  Table [dbo].[RegistryUploadIngredients]    Script Date: 9/13/2019 10:19:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistryUploadIngredients](
	[pKey] [uniqueidentifier] NOT NULL,
	[pKeyPurpose] [uniqueidentifier] NULL,
	[IngredientName] [varchar](150) NOT NULL,
	[CASNumber] [varchar](20) NOT NULL,
	[PercentHighAdditive] [float] NULL,
	[PercentHFJob] [float] NULL,
	[IngredientComment] [varchar](500) NULL,
	[IngredientMSDS] [bit] NOT NULL,
	[MassIngredient] [float] NULL,
	[ClaimantCompany] [nvarchar](max) NULL,
	[pKeyDisclosure] [uniqueidentifier] NOT NULL,
	[ImportGUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_RegistryUploadIngredients_1] PRIMARY KEY NONCLUSTERED 
(
	[pKey] ASC,
	[ImportGUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_ImportGUID]    Script Date: 9/13/2019 10:19:33 PM ******/
CREATE NONCLUSTERED INDEX [IX_ImportGUID] ON [dbo].[RegistryUploadIngredients]
(
	[ImportGUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
