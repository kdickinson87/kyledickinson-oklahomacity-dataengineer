USE [FracFocusStaging]
GO
/****** Object:  Table [dbo].[RegistryUpload]    Script Date: 9/13/2019 10:19:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistryUpload](
	[pKey] [uniqueidentifier] NOT NULL,
	[JobStartDate] [datetime] NULL,
	[JobEndDate] [datetime] NULL,
	[APINumber] [varchar](14) NOT NULL,
	[StateNumber] [varchar](2) NULL,
	[CountyNumber] [varchar](3) NULL,
	[OperatorName] [varchar](55) NULL,
	[WellName] [varchar](150) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Projection] [varchar](5) NOT NULL,
	[TVD] [float] NULL,
	[TotalBaseWaterVolume] [float] NULL,
	[TotalBaseNonWaterVolume] [float] NULL,
	[StateName] [varchar](50) NULL,
	[CountyName] [varchar](50) NULL,
	[FFVersion] [real] NULL,
	[FederalWell] [bit] NULL,
	[IndianWell] [bit] NOT NULL,
	[Source] [nvarchar](max) NULL,
	[DTMOD] [datetime] NULL,
	[ImportGUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_RegistryUpload] PRIMARY KEY NONCLUSTERED 
(
	[pKey] ASC,
	[ImportGUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_ImportGUID]    Script Date: 9/13/2019 10:19:33 PM ******/
CREATE NONCLUSTERED INDEX [IX_ImportGUID] ON [dbo].[RegistryUpload]
(
	[ImportGUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
