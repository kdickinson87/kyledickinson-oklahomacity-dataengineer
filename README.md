# KyleDickinson-OklahomaCity-DataEngineer

Below are the results to my personality test and the response question, followed by an outline of the other files within the repo. If you have any questions or need clarification, please
don't hesistate to email or call me. 



### 16Personalities Screening Results: 

https://www.16personalities.com/profiles/d459cc8a2e1a4

#### My take on the results:

Overall it's pretty accurate I'd say. I'm pretty outgoing, and love thinking outside the box and testing boundries. I actually took the test about a year and a half ago, and it gave the same results. 
I forgot about it, and tried to email myself the results I got this time and was told I couldn't use my email, 
as I'd already taken it before. So I went back in my email to reconsile the differences, and sure enough, there it was, same exact personality type.

#### Free Response Question:

For me, collaberation and teamwork are very important. Being that I am somewhat extroverted, I enjoy working hard together, 
and helping others when they have questions/need assistance. I am passionate about what I do, and I like to be surrounded 
by like minded co-workers who share that passion for development and data. I also enjoy team outings, happy hours, things 
of that sort. It's really just the work hard, play hard mindset. I also enjoy an environment where continued education and 
learning are encouraged through team led training sessions, usergroups, industry conferences, etc.

----------------------------

This repo consists of the following structure:

* Resume

* FracFocusStagingDB
	* Contains all FracFocusStaging database Schema

* FracFocusReportingDB
	* Contains all FracFocusReporting database Schema

* PowershellScripts
	* FracFocusDownload.ps1
		* Downloads, extracts, and restores FracFocus DB from web 
	* OCCDataDownload.ps1
		* Downloads and extracts the W27base dataset from OCC  
	* FracFocusCleanup.ps1
		* Simple script to drop the FracFocus database after import is complete
	* OCCDataCleanup.ps1
		* Simple script to delete the OCCData file after import is complete  
  

`  


* FracFocusImport - This is the SSIS solution. Contains 7 SSIS Packages
	* RegistryStagingImport
        * Imports FracFocus Registry data into Staging DB from Downloaded DB
	* RegistryReportingImport
        * Imports raw FracFocus Registry data into Reporting DB from Staging DB
	* OCCDataReportingImport
        * Imports OCC Data into Reporting DB from Staging DB
	* OCCDataStagingImport
        * Imports OCC Data into Staging DB from CSV File
	* BaseWaterVolumeByStateCountyWell
        * Copies Transformed data from view within Staging DB to Reporting DB
	* MinMaxIngredientsPerWell
        * Copies Transformed data from view within Staging DB to Reporting DB
	* JobCompletionsBySupplierWellMonth
        * Copies Transformed data from view within Staging DB to Reporting DB


-----------------------------

Notes/Assumptions:

Due to data quality issues, the final resultsets in the reporting datasets will not be 100% accurate, but with more time, data quality and cleansing would be hashed out prior pushing to a production reporing system.

I built the infrastructure in such a way as to decouple the ETL process from the Environments themselves, save for the views within the Staging database. My usual approach is to break things apart into sizable 
chunks that are easier to troubleshoot and rerun, as opposed to having one massive process that does everything within one file/ssis package/etl process. It can make for a larger number of SSIS packages, but with proper
organization it is easier to manage higher numbers of SSIS packages, than to troubleshoot/rerun a subset of an SSIS package that does everything under one roof.

Due to time constraints and the limitations of my personal laptop, I was unable to get SQL Server Agent configured properly to actually execute the jobs, 
but my vision would be to have a collection of jobs scheduled, probably based on datasets, so that they could be run in parallel. 

It depends on scheduling software how the jobs would be configured. Here at BOK we have AWA, which allows for file/directory listeners, data driven job runs, scheduled jobs, etc, so for that setup it could potentially 
be feasable to have a listener set up on the download directory, and just execute the import whenever the file drops in. But for the sake of this exercise, I'm assuming the scheduler would be SQL Server Agent, 
so the following example would be an Agent job layout.

Ex: 

1. Job1 - Execute FracFocusDownload.ps1, Execute RegistryStagingImport, Execute RegistryReportingImport, Execute FracFocusCleanup.ps1
1. Job2 - Execute OCCDataDownload.ps1, Execute OCCDataStagingIMport, Execute OCCDataReportingImport, Execute OCCDataCleanup.ps1
