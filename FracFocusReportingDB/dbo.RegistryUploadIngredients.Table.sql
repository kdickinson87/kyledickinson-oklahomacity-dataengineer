USE [FracFocusReporting]
GO
/****** Object:  Table [dbo].[RegistryUploadIngredients]    Script Date: 9/13/2019 10:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistryUploadIngredients](
	[DataDate] [date] NOT NULL,
	[pKey] [uniqueidentifier] NOT NULL,
	[pKeyPurpose] [uniqueidentifier] NULL,
	[IngredientName] [varchar](150) NOT NULL,
	[CASNumber] [varchar](20) NOT NULL,
	[PercentHighAdditive] [float] NULL,
	[PercentHFJob] [float] NULL,
	[IngredientComment] [varchar](500) NULL,
	[IngredientMSDS] [bit] NOT NULL,
	[MassIngredient] [float] NULL,
	[ClaimantCompany] [nvarchar](max) NULL,
	[pKeyDisclosure] [uniqueidentifier] NOT NULL,
	[ImportGUID] [uniqueidentifier] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [CCX_RegistryUploadIngredients]    Script Date: 9/13/2019 10:21:18 PM ******/
CREATE CLUSTERED COLUMNSTORE INDEX [CCX_RegistryUploadIngredients] ON [dbo].[RegistryUploadIngredients] WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RegistryUploadIngredients] ADD  CONSTRAINT [DF_RegistryUploadIngredients_DataDate]  DEFAULT (getdate()) FOR [DataDate]
GO
