USE [FracFocusReporting]
GO
/****** Object:  Table [dbo].[JobCompletionsBySupplierWellMonth]    Script Date: 9/13/2019 10:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobCompletionsBySupplierWellMonth](
	[DataDate] [date] NOT NULL,
	[Supplier] [varchar](150) NULL,
	[APINumber] [varchar](14) NOT NULL,
	[JobStartMonth] [datetime] NULL,
	[JobCount] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Index [CX_DataDate]    Script Date: 9/13/2019 10:21:18 PM ******/
CREATE CLUSTERED INDEX [CX_DataDate] ON [dbo].[JobCompletionsBySupplierWellMonth]
(
	[DataDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
