USE [FracFocusReporting]
GO

/****** Object:  Table [dbo].[MinMaxIngredientsPerWell]    Script Date: 9/14/2019 12:28:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MinMaxIngredientsPerWell](
	[DataDate] [date] NOT NULL,
	[APINumber] [varchar](14) NOT NULL,
	[MinPercentage] [float] NULL,
	[MinIngredient] [varchar](150) NOT NULL,
	[MaxPercentage] [float] NULL,
	[MaxIngredient] [varchar](150) NOT NULL
) ON [PRIMARY]
GO


