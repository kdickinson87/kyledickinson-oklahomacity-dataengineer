USE [FracFocusReporting]
GO
/****** Object:  Table [dbo].[RegistryUploadPurpose]    Script Date: 9/13/2019 10:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistryUploadPurpose](
	[DataDate] [date] NOT NULL,
	[pKey] [uniqueidentifier] NOT NULL,
	[pKeyRegistryUpload] [uniqueidentifier] NOT NULL,
	[TradeName] [varchar](250) NULL,
	[Supplier] [varchar](150) NULL,
	[Purpose] [varchar](250) NOT NULL,
	[SystemApproach] [bit] NULL,
	[IsWater] [bit] NULL,
	[PercentHFJob] [float] NULL,
	[IngredientMSDS] [bit] NULL,
	[ImportGUID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Index [CCX_RegistryUploadPurpose]    Script Date: 9/13/2019 10:21:18 PM ******/
CREATE CLUSTERED COLUMNSTORE INDEX [CCX_RegistryUploadPurpose] ON [dbo].[RegistryUploadPurpose] WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RegistryUploadPurpose] ADD  CONSTRAINT [DF_RegistryUploadPurpose_DataDate]  DEFAULT (getdate()) FOR [DataDate]
GO
