USE [FracFocusReporting]
GO
/****** Object:  Table [dbo].[RegistryUpload]    Script Date: 9/13/2019 10:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistryUpload](
	[DataDate] [date] NOT NULL,
	[pKey] [uniqueidentifier] NOT NULL,
	[JobStartDate] [datetime] NULL,
	[JobEndDate] [datetime] NULL,
	[APINumber] [varchar](14) NOT NULL,
	[StateNumber] [varchar](2) NULL,
	[CountyNumber] [varchar](3) NULL,
	[OperatorName] [varchar](55) NULL,
	[WellName] [varchar](150) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Projection] [varchar](5) NOT NULL,
	[TVD] [float] NULL,
	[TotalBaseWaterVolume] [float] NULL,
	[TotalBaseNonWaterVolume] [float] NULL,
	[StateName] [varchar](50) NULL,
	[CountyName] [varchar](50) NULL,
	[FFVersion] [real] NULL,
	[FederalWell] [bit] NULL,
	[IndianWell] [bit] NOT NULL,
	[Source] [nvarchar](max) NULL,
	[DTMOD] [datetime] NULL,
	[ImportGUID] [uniqueidentifier] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [CCX_RegistryUpload]    Script Date: 9/13/2019 10:21:18 PM ******/
CREATE CLUSTERED COLUMNSTORE INDEX [CCX_RegistryUpload] ON [dbo].[RegistryUpload] WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RegistryUpload] ADD  CONSTRAINT [DF_RegistryUpload_DataDate]  DEFAULT (getdate()) FOR [DataDate]
GO
