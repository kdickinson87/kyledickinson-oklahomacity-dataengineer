USE [FracFocusReporting]
GO
/****** Object:  Table [dbo].[BaseWaterVolumeByStateCountyWell]    Script Date: 9/13/2019 10:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BaseWaterVolumeByStateCountyWell](
	[DataDate] [date] NOT NULL,
	[State] [nvarchar](25) NULL,
	[County] [nvarchar](32) NULL,
	[APINumber] [varchar](14) NOT NULL,
	[TotalBaseWaterVolume] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Index [CX_DataDate]    Script Date: 9/13/2019 10:21:18 PM ******/
CREATE CLUSTERED INDEX [CX_DataDate] ON [dbo].[BaseWaterVolumeByStateCountyWell]
(
	[DataDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
