USE [FracFocusReporting]
GO
/****** Object:  Table [dbo].[StateCountyRef]    Script Date: 9/13/2019 10:21:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StateCountyRef](
	[State] [nvarchar](25) NULL,
	[State_Code] [nvarchar](2) NULL,
	[State_FIPS_Code] [nvarchar](2) NULL,
	[State_API_Code] [nvarchar](2) NULL,
	[County] [nvarchar](32) NULL,
	[County_FIPS_Code] [nvarchar](3) NULL,
	[Combined_FIPS] [nvarchar](5) NULL,
	[Combined_API] [nvarchar](5) NULL
) ON [PRIMARY]
GO
