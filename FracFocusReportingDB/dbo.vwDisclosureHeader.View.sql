USE [FracFocusReporting]
GO

/****** Object:  View [dbo].[vwDisclosureHeader]    Script Date: 9/14/2019 8:05:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwDisclosureHeader]
AS
     SELECT RU.DataDate, 
            RU.pKey, 
            RU.JobStartDate, 
            RU.JobEndDate, 
            RU.APINumber, 
            RU.StateNumber, 
            RU.CountyNumber, 
            RU.OperatorName, 
            RU.WellName, 
            RU.Latitude, 
            RU.Longitude, 
            RU.Projection, 
            RU.TVD, 
            RU.TotalBaseWaterVolume, 
            RU.TotalBaseNonWaterVolume, 
            RU.StateName, 
            RU.CountyName, 
            RU.FFVersion, 
            RU.FederalWell, 
            RU.IndianWell, 
            RU.Source, 
            RU.DTMOD, 
            RU.ImportGUID, 
            OWA.WellName AS OCCWellName, 
            OWA.WellNumber AS OCCWellNumber, 
            OWA.WellStatus, 
            OWA.Spud, 
            OWA.FirstProd
     FROM dbo.RegistryUpload RU
          LEFT OUTER JOIN dbo.OCCWellAttributes OWA ON OWA.APINumber = RU.APINumber
                                                       AND OWA.DataDate = RU.DataDate;
GO


