USE [FracFocusReporting]
GO

/****** Object:  Table [dbo].[OCCWellAttributes]    Script Date: 9/14/2019 8:06:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OCCWellAttributes](
	[DataDate] [date] NOT NULL,
	[APINumber] [varchar](14) NULL,
	[WellName] [varchar](100) NULL,
	[WellNumber] [varchar](16) NULL,
	[WellStatus] [varchar](10) NULL,
	[Spud] [date] NULL,
	[FirstProd] [date] NULL
) ON [PRIMARY]
GO


