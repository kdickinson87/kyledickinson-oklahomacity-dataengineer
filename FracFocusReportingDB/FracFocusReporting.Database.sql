USE [master]
GO
/****** Object:  Database [FracFocusReporting]    Script Date: 9/13/2019 10:21:18 PM ******/
CREATE DATABASE [FracFocusReporting]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FracFocusReporting', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\FracFocusReporting.mdf' , SIZE = 1449984KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FracFocusReporting_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\FracFocusReporting_log.ldf' , SIZE = 663552KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [FracFocusReporting] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FracFocusReporting].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FracFocusReporting] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FracFocusReporting] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FracFocusReporting] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FracFocusReporting] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FracFocusReporting] SET ARITHABORT OFF 
GO
ALTER DATABASE [FracFocusReporting] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FracFocusReporting] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FracFocusReporting] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FracFocusReporting] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FracFocusReporting] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FracFocusReporting] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FracFocusReporting] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FracFocusReporting] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FracFocusReporting] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FracFocusReporting] SET  ENABLE_BROKER 
GO
ALTER DATABASE [FracFocusReporting] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FracFocusReporting] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FracFocusReporting] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FracFocusReporting] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FracFocusReporting] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FracFocusReporting] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FracFocusReporting] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FracFocusReporting] SET RECOVERY FULL 
GO
ALTER DATABASE [FracFocusReporting] SET  MULTI_USER 
GO
ALTER DATABASE [FracFocusReporting] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FracFocusReporting] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FracFocusReporting] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FracFocusReporting] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FracFocusReporting] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'FracFocusReporting', N'ON'
GO
ALTER DATABASE [FracFocusReporting] SET QUERY_STORE = OFF
GO
ALTER DATABASE [FracFocusReporting] SET  READ_WRITE 
GO
