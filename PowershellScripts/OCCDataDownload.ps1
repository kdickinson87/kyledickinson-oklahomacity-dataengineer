﻿$DataDate = Get-Date -Format "MMddyyyy"
$ftp = “ftp://ftp.occeweb.com/OG_DATA/W27base.zip”
$ZipDirectory = “c:\DataSets\OCCData\W27base.zip"
$UnzippedDirectory = “c:\DataSets\OCCData\W27base"

#Downloads the latest OCC W27Base data file and unzips it.
try
{
    $fileuri = New-Object System.Uri($ftp)
    $client = new-object System.Net.WebClient
    $client.DownloadFile($fileuri,$ZipDirectory)

    Expand-Archive $ZipDirectory -DestinationPath $UnzippedDirectory

    $NewFileName = "W27base" + $DataDate + ".zip"

    Rename-Item -Path $ZipDirectory -NewName $NewFileName
}

Catch
{
    write-host "Failed to download and unzip file. Log additional info here..."
    throw
}

