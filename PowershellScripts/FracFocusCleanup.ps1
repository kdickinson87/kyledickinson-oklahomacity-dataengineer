﻿#Simple script to drop FracFocusRegistry DB
Try{
    invoke-sqlcmd -ServerInstance "StormCloak" -Query "Drop database FracFocusRegistry;"
}Catch{
      Write-Output 'Failed to delete database'
      throw
}