﻿$DataDate = Get-Date -Format "MMddyyyy"
$DownloadURL = “http://fracfocusdata.org/digitaldownload/fracfocusdata.zip”
$ZipDirectory = “c:\DataSets\FracFocusData\FracFocus_" + $DataDate + ".zip"
$UnzippedDirectory = “c:\DataSets\FracFocusData\FracFocus_" + $DataDate
$DatabaseName = "FracFocusRegistry"

#Downloads and extracts the latest FracFocus database backup. 
#I did not have the option for the CSV, as the CSV directory is empty when you download it from their site 
 try
{
    $client = new-object System.Net.WebClient
    $client.DownloadFile($DownloadURL,$ZipDirectory)

    Expand-Archive $ZipDirectory -DestinationPath $UnzippedDirectory
}

#Example of error handling. Would be more robust in a production system.
Catch
{
    write-host "Failed to download and unzip file. Log additional info here..."
    throw
}

#Restores database and deletes .Bak file if successful
try
{
    $sqlServerSnapinVersion = (Get-Command Restore-SqlDatabase).ImplementingType.Assembly.GetName().Version.ToString()
    $assemblySqlServerSmoExtendedFullName = "Microsoft.SqlServer.SmoExtended, Version=$sqlServerSnapinVersion, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
    
    $BackupFile = Get-ChildItem ($UnzippedDirectory + "\FracFocusData*.bak") -Name
    $BackupFilePath = $UnzippedDirectory + '\' + $BackupFile
    
    $RelocateData = New-Object "Microsoft.SqlServer.Management.Smo.RelocateFile, $assemblySqlServerSmoExtendedFullName"("FracFocusRegistry", "C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\FracFocusRegistry.mdf")
    $RelocateLog = New-Object "Microsoft.SqlServer.Management.Smo.RelocateFile, $assemblySqlServerSmoExtendedFullName"("FracFocusRegistry_Log", "C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\FracFocusRegistry_Log.ldf")
    
    Restore-SqlDatabase -ServerInstance "Stormcloak" -Database $DatabaseName -BackupFile $BackupFilePath -RelocateFile @($RelocateData,$RelocateLog)
    
    Remove-Item -path $UnzippedDirectory -Recurse
}
catch
{
    Write-Host "Failed to restore database. Additional logging/emailing would go here."
    throw
}

